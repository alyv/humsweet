const carouselMiniItems = document.querySelectorAll('.mini-car .content span');
let mIndex = 0;

const renderItems = () => {
  for (let i = 0; i < carouselMiniItems.length; i++) {
    carouselMiniItems[i].style.display = 'none';
  }
  carouselMiniItems[mIndex].style.display = 'block';
}

renderItems();

document.querySelector('.chevRight').addEventListener('click', ()=> {
  if (mIndex == carouselMiniItems.length - 1) {
    mIndex = 0;
  } else {
    mIndex++;
  }

  renderItems();
});

document.querySelector('.chevLeft').addEventListener('click', ()=> {
  if (mIndex == 0) {
    mIndex = carouselMiniItems.length - 1;
  } else {
    mIndex--;
  }

  renderItems()
});

